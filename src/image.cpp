#include"image.hpp"

Res::Res(unsigned int width, unsigned int height)
    : width(width), height(height) {}

float Res::get_ratio()
{
    return (float) width / (float) height;
}

Img::Img(Res res)
    : res(res)
{
    data = new Col*[res.width];
    for (unsigned int i = 0; i < res.width; i++)
        data[i] = new Col[res.height];
}

Img::~Img()
{
    for (unsigned int i = 0; i < res.width; i++)
        delete[] data[i];
    //delete[] data;
}