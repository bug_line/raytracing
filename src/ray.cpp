#include "ray.hpp"


Ray::Ray() {}

Ray::Ray(Vec3 direction, Vec3 origin)
    : dir(direction), orgn(origin) {}

RayField::RayField(Res res)
    : res(res)
{
    data = new Ray*[res.width];
    for (unsigned int i = 0; i < res.width; i++)
        data[i] = new Ray[res.height];
}

RayField::~RayField()
{
    for (unsigned int i = 0; i < res.width; i++)
        delete data[i];
    delete data;
}