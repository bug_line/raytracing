#pragma once
#include "image.hpp"
#include "cam.hpp"
#include "object/sphere.hpp"
#include "ray.hpp"

Img Render(Res res, Cam cam, Sphere sphere);
