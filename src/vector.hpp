#pragma once
#include <ostream>

struct Vec3 {
    float x, y, z;

    Vec3();
    Vec3(float val);
    Vec3(float x, float y, float z);

    float dot(Vec3 other) const;
    float sqrd_len() const;
    float len() const;
    Vec3 unit() const;
    Vec3 cross(Vec3 other) const;

    Vec3 operator+(Vec3 other) const;
	Vec3 operator-(Vec3 other) const;
	Vec3 operator*(float scalar) const;
	Vec3 operator/(float scalar) const;

};

std::ostream& operator<<(std::ostream& stream, const Vec3 vec3);