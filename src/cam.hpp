#pragma once
#include "vector.hpp"
#include "ray.hpp"

struct Cam {
    // Position in space
    Vec3 pos;
    // Unit vector
    Vec3 dir;
    // Unit vector
    Vec3 up;
    // Field Of View
    float fov;

    Cam(Vec3 position, Vec3 direction, Vec3 up, float fov);

    RayField ray_field(Res res) const;
};