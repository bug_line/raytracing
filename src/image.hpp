#pragma once
#include "color.hpp"

struct Res {
    unsigned int width, height;

    Res(unsigned int width, unsigned int height);
    float get_ratio();
};

struct Img {
    Res res;
    Col **data;

    Img(Res res);
    ~Img();
};