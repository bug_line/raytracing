#include "render.hpp"

Img Render(Res res, Cam cam, Sphere sphere)
{
    Img img(res);
    RayField rf = cam.ray_field(res);

    RayHit hit;

    for (unsigned int y = 0; y < res.height; y++) {
        for (unsigned int x = 0; x < res.width; x++) {
            hit = sphere.collide(rf.data[x][y]);

            if (hit.is_collide) {
                img.data[x][y] = Col(255);
            }
            else {
                img.data[x][y] = Col(0);
            }
        }
    }
}
