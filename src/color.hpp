#pragma once
#include <ostream>
#define u8 __UINT8_TYPE__

struct Col {
    u8 r, g, b ,a;

    Col();
    Col(u8 r, u8 g, u8 b, u8 a);
    Col(u8 r, u8 g, u8 b);
    Col(u8 rgb);
};

std::ostream& operator<<(std::ostream& stream, const Col col);