#include "color.hpp"

Col::Col()
    : r(0), g(0), b(0), a(255) {}

Col::Col(u8 r, u8 g, u8 b, u8 a)
    : r(r), g(g), b(b), a(a) {}

Col::Col(u8 r, u8 g, u8 b)
    : r(r), g(g), b(b), a(255) {}

Col::Col(u8 rgb)
    : r(rgb), g(rgb), b(rgb), a(255) {}

std::ostream& operator<<(std::ostream& stream, const Col col)
{
    stream << "r: "<< +col.r << "  g: " << +col.g << "  b: " << +col.b << "  a: " << +col.a;
    return stream;
}