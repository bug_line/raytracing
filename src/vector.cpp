#include "vector.hpp"
#include <math.h>

Vec3::Vec3() {}

Vec3::Vec3(float val)
        : x(val), y(val), z(val) {}

Vec3::Vec3(float x, float y, float z)
        : x(x), y(y), z(z) {}

float Vec3::dot(Vec3 other) const
{
    return x * other.x + y * other.y + z * other.z;
}

float Vec3::sqrd_len() const
{
    return x * x + y * y + z * z;
}

float Vec3::len() const
{
    return sqrt(x * x + y * y + z * z);
}

Vec3 Vec3::unit() const
{
    float sqrd_len = Vec3::sqrd_len();
    if (sqrd_len == 0)
        return Vec3(0);
    else
        return *this / sqrt(sqrd_len);
}

Vec3 Vec3::cross(Vec3 other) const
{
    return Vec3(
        y * other.z - z * other.y,
        z * other.x - x * other.z,
        x * other.y - y * other.x
    );
}

Vec3 Vec3::operator+(Vec3 other) const
{
    return Vec3(x + other.x, y + other.y, z + other.z);
}

Vec3 Vec3::operator-(Vec3 other) const
{
    return Vec3(x - other.x, y - other.y, z - other.z);
}

Vec3 Vec3::operator*(float scalar) const
{
    return Vec3(x * scalar, y * scalar, z * scalar);
}

Vec3 Vec3::operator/(float scalar) const
{
    return Vec3(x / scalar, y /scalar, z / scalar);
}

std::ostream& operator<<(std::ostream& stream, const Vec3 vec3)
{
    stream << "\nx: " <<vec3.x << "  y: " << vec3.y << "  z: " << vec3.z;
    return stream;
}