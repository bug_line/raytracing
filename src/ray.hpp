#pragma once
#include "vector.hpp"
#include "image.hpp"

struct Ray {
    Vec3 dir;
    Vec3 orgn;

    Ray();
    Ray(Vec3 direction, Vec3 origin);
};

struct RayHit {
    Vec3 point;
    Vec3 normal;
    float dist;
    bool is_collide;
};

struct RayField {
    Res res;
    Ray** data;

    RayField(Res res);
    ~RayField();
};