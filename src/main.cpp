#include <iostream>
#include "vector.hpp"
#include "color.hpp"
#include "cam.hpp"
#include "render.hpp"
#include "object/sphere.hpp"

int main()
{
    Res res(
        1920,
        1080
    );
    Cam cam(
        Vec3(0, 0, -1),
        Vec3(0, 0, 1),
        Vec3(0, 1, 0),
        5
    );
    Sphere sphere(
        Vec3(0, 1, 0),
        0.5
    );
    Render(res, cam, sphere);
}