#include "sphere.hpp"
#include <math.h>

Sphere::Sphere() {}

Sphere::Sphere(Vec3 position, float radius)
    : pos(position), radius(radius) {}

RayHit Sphere::collide(Ray ray)
{
    RayHit ray_hit;

    float a = ray.dir.sqrd_len();
    float b = 2.0f * ray.orgn.dot(ray.dir);
    float c = ray.orgn.sqrd_len() - radius * radius;

    float discriminant = b * b - 4.0f * a * c;

    if (discriminant > 0) {
        ray_hit.dist = (-b - sqrt(discriminant)) / (2.0f * a);
        ray_hit.point = ray.orgn + ray.dir * ray_hit.dist;
        ray_hit.normal = (ray_hit.point - pos) / radius;
        ray_hit.is_collide = true;
        return ray_hit;
    }
    else if (discriminant == 0) {
        ray_hit.dist = -b / (2.0f * a);
        ray_hit.point = ray.orgn + ray.dir * ray_hit.dist;
        ray_hit.normal = (ray_hit.point - pos) / radius;
        ray_hit.is_collide = true;
        return ray_hit;
    }
    else {
        ray_hit.is_collide = false;
        return ray_hit;
    }
}