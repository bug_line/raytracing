#pragma once
#include "../vector.hpp"
#include "../ray.hpp"

struct Sphere {
    Vec3 pos;
    float radius;

    Sphere();
    Sphere(Vec3 position, float radius);
    RayHit collide(Ray ray);
};