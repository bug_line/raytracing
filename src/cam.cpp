#include "cam.hpp"

Cam::Cam(Vec3 position, Vec3 direction, Vec3 up, float fov)
    : pos(position), dir(direction), up(up), fov(fov) {}

RayField Cam::ray_field(Res res) const
{
    float h_fov = fov / res.get_ratio();

    Vec3 right_vec = dir.cross(up);
    Vec3 up_vec = right_vec.cross(dir);

    float w_len, h_len;
    Vec3 dir_ray;

    RayField rf(res);
    for (unsigned int x = 0; x < res.width; x++) {
        w_len = fov * ((float) x / (float) res.width);
        for (unsigned int y = 0; y < res.height; y++) {
            h_len = h_fov * ((float) y / (float) res.height);
            dir_ray = (dir + right_vec * (w_len) + up_vec * (h_len)).unit();
            rf.data[x][y] = Ray(dir_ray, pos);
        }
    }
    return rf;
}